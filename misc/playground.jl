using LinearAlgebra: eigvals, eigvecs, tr, exp, ⋅
using Random: rand

#----------------------------------------

"Return fluorescence response at 𝑡 = 0 using the given inital `μ` and equilibrium `ρ`."
function flresponse(μ::AbstractArray{<:Number,3}, ρ::AbstractMatrix{<:Number})
    μ² = μ[:, :, 1]*μ[:, :, 1] .+ μ[:, :, 2]*μ[:, :, 2] .+ μ[:, :, 3]*μ[:, :, 3];
    tr(μ² * ρ) / 3
end

# the last dimension is the Cartesian component (x, y, z)
μ = zeros(3, 3, 3)
μ[2, 1, :] = μ[1, 2, :] = [1 0 0];
μ[3, 1, :] = μ[1, 3, :] = [0 1 0];

ρ = [0 0        0;
     0 0.8      0.204039;
     0 0.204039 0.2];

flresponse(μ, ρ)

#----------------------------------------

"""
Convert the given `μ` from the site basis to exciton basis using the site basis `H`.
Print and return the result.
"""
function μtoEB(H::AbstractMatrix{<:Number}, μ::AbstractArray{<:Number,3})
    dim = size(H, 1) + 1
    U = zeros(dim, dim)
    U[1, 1] = 1
    U[2:end, 2:end] = eigvecs(H)

    μEB = similar(μ)
    for i = 1:3
        μEB[:, :, i] = U' * μ[:, :, i] * U
    end
    for a = 1:dim
        for b = 1:dim
            print(round.(μEB[a, b, :], digits=2))
        end
        println()
    end
    μEB
end

H = [0  50;
     50 100]

θ = 0.5atan(2H[1, 2] / (H[1, 1] - H[2, 2]))
μ = zeros(3, 3, 3)
μ[2, 1, :] = μ[1, 2, :] = [1      0 0];
μ[3, 1, :] = μ[1, 3, :] = [tan(θ) 0 0]; # this choice should give zero transition dipole moment from excitonic state 2

μEB = μtoEB(H, μ)

# print magnitudes of the transition moments to the ground states 
for i = 2:3
    println(μEB[i, 1, :] ⋅ μEB[i, 1, :])
end

#----------------------------------------

"Return effective Hamiltonian."
function heff(β::Real, λ::AbstractVector{<:Real}, H::AbstractMatrix{<:Number})
    dim = length(λ)
    Λ = zeros(dim, dim)
    for i = 1:dim
        Λ[i, i] = λ[i]
    end
    e = exp(-β*Λ/6)
    H_ε = copy(H)
    H_J = copy(H)
    for I in CartesianIndices(H_ε)
        I[1] == I[2] ? H_J[I] = 0 : H_ε[I] = 0
    end
    H_ε - Λ + e*H_J*e
end

T = 300
k_B = 0.69503476
β = 1 / (k_B * T)
λ = [10, 10, 100]
H = [0   50  100;
     50  100 100;
     100 100 200]

# λ = [10, 10]
# H = [100 100;
#      100 0]

H_eff = heff(β, λ, H)
E = exp(-β * H_eff)
ρ = E / tr(E)   # the density matrix in the site basis

ε = eigvals(H_eff)
e = exp.(-β.*ε)
pops = e ./ sum(e) # populations of the effective states; equivalently, the diagonal of ρ in the global basis

# μ = zeros(3, 3, 3)
# μ[2, 1, :] = μ[1, 2, :] = [1 0 0];
# μ[3, 1, :] = μ[1, 3, :] = [0 1 0];
# μ[2, 2, :] = [1.5 0   0];
# μ[3, 3, :] = [0   1.5 0];

μ = zeros(4, 4, 3)
μ[2, 1, :] = μ[1, 2, :] = [1 0 0]; μ[2, 2, :] = [1.5 0   0];
μ[3, 1, :] = μ[1, 3, :] = [0 1 0]; μ[3, 3, :] = [0   1.5 0];
                                   μ[4, 4, :] = [0   0   15];

μEB = μtoEB(H_eff, μ)

# print magnitudes of the transition moments to the ground states 
for i = 2:size(μEB, 1)
    println(μEB[i, 1, :] ⋅ μEB[i, 1, :])
end

#----------------------------------------

"""
Calculate the equilibrium ρ using the effective coupling with no external field.
Then, calculate ρ's with different orientations of the external field.
Average them and compare with the initial ρ.
"""
function ρmean(H, λ, μ22; N=1e6)
    T = 300
    k_B = 0.69503476
    β = 1 / (k_B * T)
    H_eff = heff(β, λ, H)
    # print("H_eff: $H_eff\n")
    H_eff_free = copy(H_eff)
    E_stark = [0, 1/√2, 1/√2] * 3*16.792
    μ11 = [1.5, 0, 0]
    E = exp(-β*H_eff_free)
    ρ0 = E / tr(E)
    print("Initial:\n$ρ0\n")

    ρ = zeros(2, 2)
    Φ = zeros(3, 3)
    for _ in 1:N
        θ = setΦ!(Φ)
        H_eff[1, 1] = H_eff_free[1, 1] - (Φ * μ11) ⋅ E_stark
        H_eff[2, 2] = H_eff_free[2, 2] - (Φ * μ22) ⋅ E_stark
        # print("H_eff: $H_eff\n")
        E = exp(-β * H_eff)
        ρ += E/tr(E) * sin(θ)
    end
    ρ .= ρ/N * π/2
    print("Final:\n$ρ\n")
end
λ = [10, 10]
H = [100 100;
     100 0]
μ22 = [0, 15, 0]

ρmean(H, λ, μ22, N=1e6)

"Generate a random converting matrix Φ in-place, and return the angle θ."
function setΦ!(Φ)
    θ = π * rand()
    ϕ = 2π * rand()
    χ = 2π * rand()
    cosθ = cos(θ); cosϕ = cos(ϕ); cosχ = cos(χ);
    sinθ = sin(θ); sinϕ = sin(ϕ); sinχ = sin(χ);
    Φ[1, 1] =  cosϕ*cosθ*cosχ - sinϕ*sinχ;
    Φ[1, 2] = -cosϕ*cosθ*sinχ - sinϕ*cosχ;
    Φ[1, 3] =  cosϕ*sinθ;
    Φ[2, 1] =  sinϕ*cosθ*cosχ + cosϕ*sinχ;
    Φ[2, 2] = -sinϕ*cosθ*sinχ + cosϕ*cosχ;
    Φ[2, 3] =  sinϕ*sinθ;
    Φ[3, 1] = -sinθ*cosχ;
    Φ[3, 2] =  sinθ*sinχ;
    Φ[3, 3] =  cosθ;
    θ
end

#----------------------------------------

"Calculate initial Stark fluorescence response."
function sfresponse(H, λ, μ02, μ22; N=1e6)
    T = 300
    k_B = 0.69503476
    β = 1 / (k_B * T)
    H_eff = heff(β, λ, H)
    # print("H_eff: $H_eff\n")
    H_eff_free = copy(H_eff)
    E_stark = [0, 1/√2, 1/√2] * 3*16.792
    E_light = [1/√3, √(2/3), 0]
    μ01 = [1, 0, 0]
    μ11 = [1.5, 0, 0]
    E = exp(-β*H_eff_free)
    ρ_temp = E / tr(E)
    ρ0 = zeros(3, 3);
    ρ0[2:3, 2:3] = ρ_temp
    print("Initial:\n$ρ0\n")

    ρ = zeros(3, 3)
    Φ = zeros(3, 3)
    μ = zeros(3, 3)
    R = 0.0
    for _ in 1:N
        θ = setΦ!(Φ)
        H_eff[1, 1] = H_eff_free[1, 1] - (Φ * μ11) ⋅ E_stark
        H_eff[2, 2] = H_eff_free[2, 2] - (Φ * μ22) ⋅ E_stark
        E = exp(-β * H_eff)
        ρ_temp = E/tr(E)
        ρ[2:3, 2:3] = ρ_temp
        μ[1, 2] = μ[2, 1] = (Φ * μ01) ⋅ E_light
        μ[1, 3] = μ[3, 1] = (Φ * μ02) ⋅ E_light
        R += tr( μ * μ * (ρ-ρ0) * sin(θ))
    end
    R = R/N * π/2
    println("R: $R")
    R
end

λ = [10, 200]
H = [100  100;
     100  0]
μ02 = [0, 0, 0]
μ22 = [0, 15, 0]

fs = sfresponse(H, λ, μ02, μ22, N=1e5)