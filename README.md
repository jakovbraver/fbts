# QuaCla User Manual

## Launching the program

### OpenMP version

Usage:

`$ quacla [options]`

Options:
* `-j <threads-count>` — number of threads (jobs) to spawn. Defaults to the maximum available number of threads.
* `-f <input-file>` — input filename. Defaults to "input.json".

Note that a very simple argument parser is used, so a space between the flag and the corresponding value is mandatory.

### Distributed version

The distributed version is intended to be launched via a job scheduling system using the provided `go.*` scripts:
* PBS/Torque:
    
    `$ qsub go.pbs`
* Slurm:
    
    `$ sbatch go.slurm`

The scripts launch the calculation via an intermediate `launch_quacla.sh` script. There, unique process ID's are available, and the program is launched as follows:

`$ quacla <thread-id> <threads-count> [-f <input-file>]`

Arguments:
* `<thread-id>` — a unique ID of the current thread (process). Used as a seed for the random number generator and as a part of the name of a temporary output file.
* `<threads-count>` — the total number of threads (processes) among which the work will be distributed. Used to calculate the number of iterations of the MC loop that every thread should perform.
* `-f <input-file>` — input filename. Defaults to "input.json".

Each thread outputs its results to a temporary file `<thread-id>_<target>.temp`, where `<target>` is "rho_S" if calculating dynamics, or the `calculation_target` input parameter if calculating a spectrum.

The controlling script waits until all threads finish the calculation and calls the reduction program:

`$ reduction 0_<target>.temp... [-f <input-file>]`

Once the reduction completes, the final files are created, and the temporary files are removed.

## Input parameters

The computation parameters should be specified in a JSON file, "input.json" by default (see sample input files in `inputs/`). All parameters are not case-sensitive.
    
Parameters explained:
* `reference_execution_time_[s]` — used to estimate execution time. See the last section for details.
* `calculation_target`:
    * `d` for density matrix dynamics (see [[1]][FBTS-bench]);
    * `a` for absorption (see [[2]][AbsFl]);
    * `f` for fluorescence (see [[2]][AbsFl]);
    * `ae`/`fe` for absorption/fluorescence in the presence of an external electric field;
    * `as`/`fs` for Stark absorption/fluorescence (see [[3]][Stark]).
* `calculation_method` — `fbts` [[4]][FBTS], `pbme` [[5]][PBME], or `pbme-nh` [[6]][PBME-nH].
* `trajectories_count` — number of Monte Carlo samples to perform.
* `relative_tolerance` — relative tolerance to use in the RK45 solver. If set to `0`, the tolerance is calculated automatically as `sqrt(trajectories_count * 10)`. This, however, may be inadequate for large systems (~7 electronic levels) when propagating for more than ~2 ps, as is most obvious from the calculation of dynamics. A safer value is `1e-6`, but note that calculating using `1e-6` will take longer than using, e.g., `1e-3`.
* `system_hamiltonian_[1/cm]` — the site-basis Hamiltonian of the electronic system in the matrix form (nested JSON arrays). When calculating dynamics (directly or during a fluorescence calculation), the first site-basis electronic state is assumed to be excited initially. For example, for a two-level system with levels numbered 1 and 2, the state with energy *H*<sub>11</sub> is initally excited.
* `energy_disorder_[1/cm]` — standard deviations of the Gaussian disorder of the system energies (diagonal values of the system Hamiltonian). To turn off the disorder, use a vector of zeros. Note that disorder is ignored when calculating fluorescence using the effective coupling theory (see below). Support for this might be added in the future.
* `transition_dipole_moments_[D]` — excited-to-ground-state transition dipole moments — elements *μ*<sub>0*n*</sub> of the dipole moment matrix. It is assumed that *μ*<sub>0*n*</sub> = *μ*<sub>*n*0</sub> (where *n* ranges from 1 to the total number of electronic levels).
* `static_dipole_moments_[D]` — diagonal elements *μ<sub>nn</sub>* of the dipole moment matrix, where *n* ranges from 0 to the total number of electronic levels.
* `stark_field_strength_[MV/cm]` — strength of the static external electric field used for simulations of Stark spectra. The geometry of the experiment assumed in the code corresponds to Fig. 1 in [[3]][Stark]. Support for user-specified geometries might be added in the future.
* `should_use_J_eff` — whether the equilibrium reduced density matrix (RDM), required for fluorescence calculations, should be calculated using the effective coupling theory [[7]][Jeff] instead of reading from file or calculating using FBTS/PBME.
* `should_read_RDM` — whether the equilibrium RDM should be read from the input file instead of using the effective coupling theory or calculating using FBTS/PBME. This option is only intended for the `f` target, but not `fe` or `fs` because in the presence of an electric field the equilibrium depends on the orientation.
* `excited_state_RDM` — excited state equilibrium RDM, entered in the same format as used for output of the calculated density matrix (see next section). 
* `spectral_density.type` — `Debye` or `B777`. For Debye, parameter `spectral_density.reorganisation_energy_[1/cm]` corresponds to *λ* from [[1]][FBTS-bench], Eq. (24), and `spectral_density.relaxation_constant_[1/cm]` corresponds to *γ*. The spectral density B777 is hardcoded according to Eq. (2.17) in [[8]][B777] using the parameters specified therein.
* `spectral_density.reorganisation_energy_[1/cm].coefficients` — (not applicable for the B777 spectral density) each site can be set to have a different reorganisation energy using these `coefficients` which are multiplied by the `base_value_[1/cm]`.
* `discretisation_is_uniform` — may be set to `false` if non-uniform (tangential) discretisation is desired for the Debye spectral density. For B777, uniform discretisation will be used regardless of the value of this parameter.
* `temperature_[K]` — bath temperature.
* `evolution_duration_[ps]`/`response_duration_[ps]` — evolution/response function propagation duration.
* `evolution_points_count`/`response_points_count` — the number of time points where the dynamics/response should be output. The zeroth time point is included automatically, so if duration is set to `0.5` (ps) and the number of points is set to `500`, the output will include 501 points from 0 to 0.5 ps with the step size of 1 fs.<br>
You may set these parameters to `0` to get the output every 1 fs.

Note that in order to calculate fluorescence, you have to specify the equilibration duration `evolution_duration_[ps]` manually. You can also specify the duration that is less than the equilibration duration if you wish to calculate the auxiliary time-resolved fluorescence. In that case, the effective coupling theory is not directly applicable, but you can experiment with that.

See [[1]][FBTS-bench] for an FBTS/PBME/Redfield/Förster/HEOM accuracy comparison, and see the accompanying supplementary material for tips on the spectral density discretisation (uniform vs. non-uniform, number of bath oscillators, cut-off frequency).<br>
Calculation of absorption and fluorescence spectra is demonstrated in [[2]][AbsFl], while the Stark spectra are analysed in [[3]][Stark].

## Results output format

The density matrix (`rho`) dynamics is written to `<method>_rho_S.txt` using the format

    time [ps]    Re(rho11)    Im(rho11)    Re(rho12)    Im(rho12)    ...

The response function (`R`) is written to `<method>_<target>_response.txt` using the format

    time [ps]    Re(R)    Im(R)

No dimensional multiplier is used in the response function, so it comes out in the units of Debye<sup>2</sup>.

The spectrum (`S`) is written to `<method>_<target>_spectrum.txt` using the format

    frequency [1/cm]    S

The spectrum is the real part of the Fourier transform of the response function with no dimensional multipliers, and multiplication by dt is also omitted.

## Getting approximate execution time

The OpenMP program automatically prints the approximate execution time based on the input parameters.

When working with the distributed version, you can quickly get approximate execution time by running the `execution_time` program:

`$ execution_time [options]`

Options:
* `-j <threads-count>` — number of threads (jobs) to assume. Defaults to 96.
* `-f <input-file>` — input filename. Defaults to "input.json".

The execution time is calculated based on the reference time specified in the input file (`reference_execution_time_[s]`). To find your system-specific reference time, carry out a calculation for a dimer with the following parameters (use `input_dimer.json`):
    
    "trajectories_count": 1e5,
    "relative_tolerance": 0,
    "calculation_target": "d",
    "oscillators_count": 100,
    "cutoff_frequency_[1/cm]": 1000,
    "evolution_duration_[ps]": 0.5,

Only these parameters impact the computation time, and others can be set to any values.

Note that this is intended only as an order-of-magnitude estimation. To get a more precise estimation, perform a calculation using 10-100 times less trajectories, note the actual execution time, and make a prediction accordingly.

Also, the estimator always assumes that the relative tolerance is set to the default value (`0`).

[FBTS]: https://aip.scitation.org/doi/10.1063/1.4736841
[PBME]: https://aip.scitation.org/doi/10.1063/1.2971041
[PBME-nH]: https://aip.scitation.org/doi/10.1063/1.4874268
[FBTS-bench]: https://aip.scitation.org/doi/10.1063/5.0006538
[B777]: https://aip.scitation.org/doi/10.1063/1.1470200
[Jeff]: https://aip.scitation.org/doi/10.1063/1.5141519
[AbsFl]: https://pubs.acs.org/doi/10.1021/acs.jctc.1c00777
[Stark]: https://aip.scitation.org/doi/10.1063/5.0073962