#!/bin/bash
# $1 is the total number of threads
cd $PBS_O_WORKDIR
~/fbts/bin/quacla $((16#$PBS_TASKNUM)) $1
