#pragma once

#include <string>
#include <vector>
#include <random>
#include <complex>
#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>
#include <json.hpp>

class Propagator {

template <typename T>
using Vector = Eigen::Matrix<T, 1, Eigen::Dynamic>;
template <typename T>
using Matrix = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;

template <typename T>
using Array1D = Eigen::Array<T, 1, Eigen::Dynamic>;
template <typename T>
using Array2D = Eigen::Array<T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;

private:
    nlohmann::json input;
    std::string target;
    enum class Method {fbts, pbme, pbmenh};
    Method method;
    long long thread_id;
    long long N_mc; // number of Monte Carlo samples
    int N_el, N_el_actual, N_osc, system_size, bath_size, phase_space_size;
    int evolution_points_count, response_points_count;
	double gamma, omega_max; // spectral density parameters
    double beta; // inverse temperature
    Matrix<double> H_S; // system Hamiltonian, N_el x N_el
    Array1D<double> omega; // bath oscillators' frequencies, assuming the same for all sites
    Array1D<double> lambda; // reorganisation energies
    Array2D<double> d; // interaction strength parameters, N_el x N_osc
    std::vector<double> evolution_times, response_times;
    bool system_is_disordered;
    std::vector<std::normal_distribution<double>> disorder_distributions; // disorder of system energies
    void set_phase_space_size();
    void set_spectral_density_parameters();
    void set_propagation_durations();
#ifdef USE_OMP
    auto estimate_execution_time() const;
    void perform_fft(const std::complex<double> R[]) const;
#endif
    auto calculate_write_density_matrix(std::normal_distribution<double> &q_p_distribution,
                                        std::vector<std::normal_distribution<double>> &Q_P_distributions,
                                        double rel_tol);
    void calculate_response(std::complex<double> R[], std::normal_distribution<double> &q_p_distribution,
                            std::vector<std::normal_distribution<double>> &Q_P_distributions,
                            double rel_tol);
public:
    Propagator(const std::string &input_filename, long long thread_number, int threads_count);
    void read_input(const std::string &input_filename, int threads_count);
    void calculate();
};
