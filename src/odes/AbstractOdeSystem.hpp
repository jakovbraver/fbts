#pragma once

#include <vector>
#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>

class AbstractOdeSystem {

template <typename T>
using Vector = Eigen::Matrix<T, 1, Eigen::Dynamic>;
template <typename T>
using Matrix = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;

template <typename T>
using Array1D = Eigen::Array<T, 1, Eigen::Dynamic>;
template <typename T>
using Array2D = Eigen::Array<T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;

template <typename T>
using Tensor = Eigen::Tensor<T, 3, Eigen::RowMajor>;

protected:
	bool gnd_included, skip_level0;
	int N_el, N_osc;
	// reference to the system Hamiltonian, which may change if disorder is present
	Matrix<double> &H_S;
	double gamma;
	Array1D<double> omega;
	Array2D<double> d;
	int q, p, q_prime, p_prime, Q, P;
	int indices[4];	
	// reference to `mu0_E_stark`, which will be calculated in the Propagator
	Vector<double> &mu0_E_stark;
	// array of diagonal elements of Hamiltonian h, Eq. (16) in J. Chem. Phys. 152, 214116 (2020) [https://doi.org/10.1063/5.0006538]
	Vector<double> diagonal;

public:	
	AbstractOdeSystem(bool gnd_included_, bool skip_level0_, int N_el_, int N_osc_, Matrix<double> &H_S_,
					  double gamma_, const Array1D<double> &omega_, const Array2D<double> &d_, Vector<double> &mu0_E_stark_):
		gnd_included{gnd_included_}, skip_level0{skip_level0_}, N_el{N_el_}, N_osc{N_osc_}, H_S{H_S_}, gamma{gamma_},
		omega{omega_}, d{d_}, mu0_E_stark{mu0_E_stark_} {
			q = skip_level0;
			p = q + (N_el + skip_level0);
			q_prime = p + (N_el + skip_level0);
			p_prime = q_prime + (N_el + skip_level0);
			Q = p_prime + N_el;
			P = Q + (N_el-gnd_included)*N_osc;
			indices[0] = q, indices[1] = p, indices[2] = q_prime, indices[3] = p_prime;
			diagonal = Vector<double>::Zero(N_el - gnd_included_);
	}
	// Function with the equations. It is not const-qualified because `diagonal` will be modified inside.
	virtual void operator()(const std::vector<double> &y, std::vector<double> &dy, const double /* t */) = 0;
	virtual ~AbstractOdeSystem() = default;
};
/* gnd_included: indicates whether the ground state is included in the problem.
 * 	If the final goal is to get the density matrix elements, the ground state is not included in the problem at all, so
 * 	`gnd_included` should be set to false.
 * 	If the final goal is to get the spectrum, we will have to include the ground state in the calculations when propagating
 * 	the dipole moment. If we want to use the same equations both when performing calculations with and without the ground
 * 	state, some indices in the equations have to be adjusted. The reason is that for a dimer, H_S is stored in a 2x2 matrix,
 * 	which is logical when density matrix is calculated (and the ground state is ignored), but when the ground state is important,
 * 	the equations are slightly different. This requires adding of subtracting a 1 in several places, so we use the boolean
 * 	`gnd_included` variable there.
 * 
 * skip_level0: tells whether energy level 0 should be skipped.
 * 	In case of excited state fluorescence, we first need to find the excited state equilibrium,
 * 	and when calculating that, the ground state is ignored. This means that the `y` vector does contain the ground state 
 * 	coordinates: y = {q0, q1, q2, p0, p1, p2, q'0, q'1, q'2, p'0, p'1, p'2, Q11, Q12, Q21, Q22, P11, P12, P21, P22}, but we
 * 	ignore them by adjusting the `q`, `p`, `q_prime` and `p_prime` variables accordingly.
 * 	When the final goal is to get the density matrix, the ground state is not included in the problem at all, so `y` vector does
 * 	not contain the ground state coordinates. In this case, level 0 is not the ground state (e.g. of a dimer) -- it is just the lowest
 *  excited state. Therefore, level 0 should *not* be skipped in this case. On the other hand, when propagating
 * 	the density matrix (i.e. system and bath oscillators) for further use in propagating the dipole moment, level 0 is the gound state
 *  and we have to skip it.
 * 
 * So, when calculating just the density matrix, set
 * 	gnd_included = false, skip_level0 = false.
 * 	(The ground state is not included in the problem. Level 0 is just a "regular" level, so it should not be skipped)
 * 
 * When calulating the density matrix to get excited state equilibrium, set
 * 	gnd_included = false, skip_level0 = true.
 * 	(The ground state is not included in the problem of finding the excited state equilibrium. However, the ground state is present
 * 	 in the `y` array, and level 0 is the ground state, so it should be skipped)
 * When calulating the spectrum, set
 * 	gnd_included = true, skip_level0 = false.
 * 	(The ground state is included in the problem. Level 0 is the ground state, but it does play a role here, so it should not be skipped.)
*/