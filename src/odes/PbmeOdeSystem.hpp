#pragma once

#include "AbstractOdeSystem.hpp"

#include <vector>
#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>

class PbmeOdeSystem : public AbstractOdeSystem {

template <typename T>
using Vector = Eigen::Matrix<T, 1, Eigen::Dynamic>;
template <typename T>
using Matrix = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;

template <typename T>
using Array1D = Eigen::Array<T, 1, Eigen::Dynamic>;
template <typename T>
using Array2D = Eigen::Array<T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;

template <typename T>
using Tensor = Eigen::Tensor<T, 3, Eigen::RowMajor>;

public:
	PbmeOdeSystem(bool gnd_included_, bool skip_level0_, int N_el_, int N_osc_, Matrix<double> &H_S_,
				  double gamma_, const Array1D<double> &omega_, const Array2D<double> &d_, Vector<double> &mu0_E_stark_):
		AbstractOdeSystem{gnd_included_, skip_level0_, N_el_, N_osc_, H_S_, gamma_, omega_, d_, mu0_E_stark_} {}

	void operator()(const std::vector<double> &y, std::vector<double> &dy, const double /* t */) override {
		double trace = 0;
		for (int i = 0; i < N_el-gnd_included; i++) {
			diagonal(i) = H_S(i, i) + mu0_E_stark(i+1);
			for (int j = 0; j < N_osc; j++)
				diagonal(i) -= omega(j) * d(i, j) * y[Q + i*N_osc + j];
			trace += diagonal(i);
		}
		trace += mu0_E_stark(0);
		trace /= N_el; // normalise the trace
		// Compute the inner product needed for calculatiing `dy[P]`. Can't use `inner_product`
		// because if level 0 must be skipped, the pattern becomes irregular.
		double q_p_squared = 0;
		for (int i = 0; i < N_el; i++)
			q_p_squared += y[q + i] * y[q + i] + y[p + i] * y[p + i];
		q_p_squared = (q_p_squared - N_el) / (2.0 * N_el); // account for the "-1" in the formula which sums to "-N_el"

		double q_sum = 0, p_sum = 0; // for storing the sums of all elements except the i'th
		double A = mu0_E_stark(0) - trace;
		if (gnd_included) {
			dy[q] =  (y[p] * A + p_sum) / gamma; 
			dy[p] = -(y[q] * A + q_sum) / gamma;
		}

		for (int i = gnd_included; i < N_el; i++) {
			q_sum = 0, p_sum = 0;
			for (int j = gnd_included; j < N_el; j++) {
				if (j != i) {
					q_sum += y[q + j] * H_S(i-gnd_included, j-gnd_included);
					p_sum += y[p + j] * H_S(i-gnd_included, j-gnd_included);
				}
			}
			A = diagonal(i-gnd_included) - trace;
			dy[q + i] =  (y[p + i] * A + p_sum) / gamma; 
			dy[p + i] = -(y[q + i] * A + q_sum) / gamma;

			double brackets = (y[q + i]*y[q + i] + y[p + i]*y[p + i] - 1.0)/2.0 + 1.0/N_el - q_p_squared;
			int a = i - gnd_included;
			for (int j = 0; j < N_osc; j++) {
				dy[Q + a*N_osc + j] =  omega(j)/gamma *  y[P + a*N_osc + j];
				dy[P + a*N_osc + j] = -omega(j)/gamma * (y[Q + a*N_osc + j] - d(a, j) * brackets);	
			}	
		}
		if (skip_level0)
			dy[q - 1] = dy[p - 1] = 0.0;
	}
};
