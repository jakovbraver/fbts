#define _USE_MATH_DEFINES 

#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <cmath>
#include <complex>
#include <utility> // swap
#include <fftw3.h>
#include <json.hpp>
#include <Eigen/Dense>
#include "utils.hpp" // parse_args, to_lower

template <typename T>
using Matrix = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;

/* Reduction program for the distributed version.
 * Usage: reduction.exe 0.temp... [-f <file>]
 * Options:
 *      -f <file>	Input filename. Defaults to "input.json".
 */
int main(int argc, const char *argv[]) {
	std::string input_filename = "input.json";
    auto flag_map = utils::parse_args(argc, argv);
    if (flag_map.count("-f")) {
		input_filename = flag_map["-f"];
        argc -= 2; // `argc` will indicate the number of files, but arguments "-f <file>" are not reduction files
    }
	std::ifstream in{input_filename};
    nlohmann::json input;
	in >> input;

    std::string target = input.at("calculation_target");
    utils::to_lower(target);
    std::string method = input.at("calculation_method");
    utils::to_lower(method);

    const auto threads_count = argc - 1; // the very first argument is the program name, hence `-1`
    
    if (target == "d") {
        // the user may enter 0 for the number of time points to get output every 1 fs; otherwise use the provided number
        const int evolution_points_count = int(input.at("evolution_points_count")) == 0 ? double(input.at("evolution_duration_[ps]")) * 1000 + 1
                                                                                        : int(input.at("evolution_points_count")) + 1; // +1 to include the initial moment
        std::vector<double> evolution_times(evolution_points_count);

        const int N_el = input.at("system_hamiltonian_[1/cm]").size();
        // number of columns in the final output
        const auto size = 2 * N_el*N_el;

        Matrix<double> rho_S_mean = Matrix<double>::Zero(evolution_points_count, size);

        // read all files and calulate the mean
        std::ifstream mean_istream;
        for (int i = 1; i < argc; i++) {
            mean_istream.open(argv[i]);
            double mean_element;
            for (int j = 0; j < evolution_points_count; j++) {  // iterate through time moments
                mean_istream >> evolution_times[j];
                for (int k = 0; k < size; k++) { // iterate through matrix elements
                    mean_istream >> mean_element;
                    rho_S_mean(j, k) += mean_element;
                }
            }
            mean_istream.close();
        }
        rho_S_mean /= threads_count;
        
        std::ofstream mean_ostream{method + "_rho_S.txt"};
        mean_ostream.precision(7);
        for (int i = 0; i < evolution_points_count; i++) {
            mean_ostream << evolution_times[i];
            for (int k = 0; k < size; k++)  // iterate through matrix elements
                mean_ostream << " " << rho_S_mean(i, k);
            mean_ostream << "\n";
        }
        mean_ostream.close();
    } else { // target is spectrum
        const double time_interval = input.at("response_duration_[ps]");
        const int response_points_count = int(input.at("response_points_count")) == 0 ? double(input.at("response_duration_[ps]")) * 1000 + 1
                                                                                      : int(input.at("response_points_count")) + 1; // +1 to include the initial moment
        const auto dt = time_interval / (response_points_count - 1);                                                                                   
        std::vector<double> response_times(response_points_count);

        // The response function will be padded with zeros before transforming.
		// For that, find the next 5th power of 2 for `evolution_points_count` -- this will be the number of points used for FFT.
        const auto N_fft = ( 1 << (5 + static_cast<int>(std::log2(response_points_count))) );
        // Allocate memory to hold the padded response function and later its transform
		auto R = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N_fft); // C-style allocation as recommended for FFTW
        for (int i = 0; i < N_fft; i++)	// initialise `R` with zeros
			R[i][0] = R[i][1] = 0.0;

        std::ifstream response_istream;
        for (int i = 1; i < argc; i++) {
            response_istream.open(argv[i]);
            double R_element;
            for (int j = 0; j < response_points_count; j++) {  // iterate through time moments
                response_istream >> response_times[j];
                response_istream >> R_element; // read the real part
                R[j][0] += R_element; 
                response_istream >> R_element; // read the imaginary part
                R[j][1] += R_element; 
            }
            response_istream.close();
        }

        // calculate the mean
        for (int i = 0; i < response_points_count; i++)	{ 
			R[i][0] /= threads_count;
            R[i][1] /= threads_count;
        }

        // output response function
        std::ofstream out{method + "_" + target + "_response.txt"};
        out.precision(6);
        for (int i = 0; i < response_points_count; i++)
            out << response_times[i] << " " << R[i][0] << " " << R[i][1] << "\n";
        out.close();

        // perform FFT
		const auto FFT_DIRECTION = (target[0] == 'a' ? FFTW_BACKWARD : FFTW_FORWARD);
        auto plan = fftw_plan_dft_1d(N_fft, R, R, FFT_DIRECTION, FFTW_ESTIMATE); // in-place transform
		fftw_execute(plan);
		fftw_destroy_plan(plan);

		for (int i = 0; i < N_fft/2; i++) // perform "fftshift"
			std::swap(R[i][0], R[i + N_fft/2][0]);

		// prepare frequencies
        const auto ps_cm_converter = 5.308375,	// ps to 1/cm conversion factor
		           f_max = (M_PI / dt) * ps_cm_converter, // convert to 1/cm
				   df = 2.0 * f_max / N_fft;
		std::vector<double> frequencies(N_fft);
		for (int i = 0; i < N_fft; i++)
			frequencies[i] = (-f_max + df) + i * df;

		out.open(method + "_" + target + "_spectrum.txt");
		for (int i = 0; i < N_fft; i++)
			out << frequencies[i] << " " << R[i][0] << "\n";	
		out.close();

		fftw_free(R);
    }

    return 0;
}
