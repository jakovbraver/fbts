#pragma once

#include <cmath>
#include <vector>
#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>

class ConverterToLabFrame {

template <typename T>
using Tensor = Eigen::Tensor<T, 3, Eigen::RowMajor>;

private:
    Eigen::Matrix3d Phi;

public:
    ConverterToLabFrame(double theta, double phi, double chi) {
        set_Phi(theta, phi, chi);
    }

    /* Construct the direction cosine matrix for switching to the Lab frame.
	 * It is meant to operate by pre-multiplying column vectors and is the transposed version
	 * of Eq. (3) in J. Chem. Educ. 81, 877 (2004) [https://doi.org/10.1021/ed081p877]
	 */
	void set_Phi(double theta, double phi, double chi) {
		auto cos_theta = std::cos(theta), cos_phi = std::cos(phi), cos_chi = std::cos(chi);
		auto sin_theta = std::sin(theta), sin_phi = std::sin(phi), sin_chi = std::sin(chi);
		Phi(0, 0) =  cos_phi*cos_theta*cos_chi - sin_phi*sin_chi;
		Phi(0, 1) = -cos_phi*cos_theta*sin_chi - sin_phi*cos_chi;
		Phi(0, 2) =  cos_phi*sin_theta;
		Phi(1, 0) =  sin_phi*cos_theta*cos_chi + cos_phi*sin_chi;
		Phi(1, 1) = -sin_phi*cos_theta*sin_chi + cos_phi*cos_chi;
		Phi(1, 2) =  sin_phi*sin_theta;
		Phi(2, 0) = -sin_theta*cos_chi;
		Phi(2, 1) =  sin_theta*sin_chi;
		Phi(2, 2) =  cos_theta;
	}

	/* Multiply the dipole moments contained in the `mu` matrix by `Phi` to convert them to the Lab frame.
	 * The result is written to `mu_lab`.
	 */
	void convert(const Tensor<double> &mu, Tensor<double> &mu_lab) {
		int N = mu.dimension(0);
		for (int a = 0; a < N; a++) { // iterate through the rows of the dipole moment matrix
            for (int b = 0; b < N; b++) { // iterate through the columns of the dipole moment matrix
                for (int j = 0; j < 3; j++)	{ // iterate though components of each dipole moment
                    mu_lab(a, b, j) = 0;
                    for (int k = 0; k < 3; k++) // iterate though components of Phi
                        mu_lab(a, b, j) += Phi(j, k) * mu(a, b, k);
                }
            }
		}
	}
};
