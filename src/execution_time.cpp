#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <chrono>
#include <ctime>
#include <json.hpp>
#include "utils.hpp" // parse_args, to_lower

/* Execution time estimatoion program.
 * Usage: execution_time.exe [options]
 *     Options:
 *         -j <threads>    Number of threads (jobs) to assume. Defaults to 96.
 *         -f <file>	   Input filename. Defaults to "input.json".
 */
int main(int argc, const char *argv[]) {
    nlohmann::json input;
	std::string input_filename = "input.json";
    auto flag_map = utils::parse_args(argc, argv);
    if (flag_map.count("-f"))
		input_filename = flag_map["-f"];
	std::ifstream in{input_filename};
	in >> input;

    auto threads_count = 96;
	if (flag_map.count("-j"))
		threads_count = std::stoi(flag_map["-j"]);
    else
        std::cout << "The number of threads has not been passed. Assuming " << threads_count << " threads will be used.\n";

    std::string target = input.at("calculation_target");
	utils::to_lower(target);

    const int N = input.at("trajectories_count"),
              N_el = input.at("system_hamiltonian_[1/cm]").size(),
              N_osc = input.at("spectral_density").at("oscillators_count");
    const double omega_max = input.at("spectral_density").at("cutoff_frequency_[1/cm]");
    const double evolution_duration = input.at("evolution_duration_[ps]"),
		   response_duration = input.at("response_duration_[ps]");
	const double propagation_duration = (target == "d" ? evolution_duration : response_duration);
	// exponentiation of `N` accounts for the fact that bigger values of `N` induce higher ODE propagation accuracy,
	// which prolongs the overall caclulation time
	int predicted_execution_time_sec = double(input.at("reference_execution_time_[s]")) * std::pow(N/1E5, 1.15) * N_osc/100 * omega_max/1000 *
		 						       propagation_duration/0.5 * N_el/2 * 96/threads_count;
	if (target != "d") {
		// approximately account for the fact that calculation of spectra takes longer than calculation of dynamics
		predicted_execution_time_sec *= 1.5;
		// approximately account for the the time required to calculate the evolution
		if (target[0] == 'f')
			predicted_execution_time_sec += (predicted_execution_time_sec/response_duration * evolution_duration) * 0.8;
		if (target[1] == 's')
			predicted_execution_time_sec *= 2;
	}

    const auto start_time = std::chrono::high_resolution_clock::now();
	// convert `start_time` to C-style `time_t` object for convenient output
	const auto start_time_c = std::chrono::system_clock::to_time_t(start_time);
    const auto predicted_finish_time = std::chrono::system_clock::to_time_t(start_time + std::chrono::seconds(predicted_execution_time_sec));
    std::cout << "\nExecution will require approximately " <<  predicted_execution_time_sec / 3600 << " h "
													       << (predicted_execution_time_sec % 3600) / 60 << " min "
													       << (predicted_execution_time_sec % 3600) % 60 << " s\n"
              << "If submitted now, on " << std::ctime(&start_time_c)
		      << "then will finish  on " << std::ctime(&predicted_finish_time) << "\n";

    return 0;
}