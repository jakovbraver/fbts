#define _USE_MATH_DEFINES
#define EIGEN_DONT_PARALLELIZE 

#include "Propagator.hpp"

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cmath>
#include <complex>
#include <random>
#include <utility>   // swap
#include <algorithm> // move, copy_n
#include <memory>
#include <chrono>
#include <ctime>
#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>
#include <unsupported/Eigen/MatrixFunctions> // matrix exponential
#include <fftw3.h>
#include <json.hpp>
#include <boost/numeric/odeint.hpp>
#include "odes/AbstractOdeSystem.hpp"
#include "odes/FbtsOdeSystem.hpp"
#include "odes/PbmeOdeSystem.hpp"
#include "odes/PbmenhOdeSystem.hpp"
#include "ConverterToLabFrame.hpp"
#include "utils.hpp" // to_lower

#ifdef USE_OMP
#include <omp.h>
#endif

namespace odeint = boost::numeric::odeint;

using Complex = std::complex<double>;

template <typename T>
using Vector = Eigen::Matrix<T, 1, Eigen::Dynamic>;
template <typename T>
using Matrix = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;

template <typename T>
using Array1D = Eigen::Array<T, 1, Eigen::Dynamic>;
template <typename T>
using Array2D = Eigen::Array<T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;

template <typename T>
using Tensor = Eigen::Tensor<T, 3, Eigen::RowMajor>;

constexpr auto I = Complex{0, 1};
constexpr auto ps_cm_converter = 5.308375;	// ps to 1/cm conversion factor

/* An "observer" that saves each ODE propagation step. Times are not saved -- we have a predefined `times` vector for that.
 * Also, only the system coordinates (q, p, q', p') are saved because only they are needed in the FBTS/PBME formula.
 */
struct SaveStep {
	Array2D<double> &states;
	const int system_size;
	int time_counter{0}; // counts the time moment so that each state is placed in the next row in `states`

	SaveStep(Array2D<double> &states_, int system_size_): states{states_}, system_size{system_size_} {}

	void operator()(const std::vector<double> &state, double /* time */) {
		for (int i = 0; i < system_size; i++)
			states(time_counter, i) = state[i];
		time_counter++;
	}
};

/* Fill a given Eigen::Matrix with the contents of a given "matrix" based on std::vector. */
template <typename T>
void eigen_matrix_from_stl(Matrix<T> &eigen_matrix, const std::vector<std::vector<T>> &stl_matrix) {
	for (size_t i = 0; i < stl_matrix.size(); i++)
		for (size_t j = 0; j < stl_matrix[0].size(); j++)
			eigen_matrix(i, j) = stl_matrix[i][j];
}

/* Construct a Propagator object. */
Propagator::Propagator(const std::string &input_filename, long long thread_id_, int threads_count): thread_id{thread_id_} {
	read_input(input_filename, threads_count);
}

/* Read the input file and initialise the corresponding fields. */
void Propagator::read_input(const std::string &input_filename, int threads_count) {
	std::ifstream in{input_filename};
	in >> input;

#ifdef USE_OMP
	if (threads_count > 0) // if the number of threads has been passed
		omp_set_num_threads(threads_count);
	N_mc = input.at("trajectories_count");
#else
	N_mc = input.at("trajectories_count");
	N_mc /= threads_count;
#endif

    set_phase_space_size();
	set_spectral_density_parameters();
	set_propagation_durations();
}

/* Set calculation target, system Hamiltonian, number of sites and oscillators, phase space size. */
void Propagator::set_phase_space_size() {
    target = input.at("calculation_target"); // "d", "a", "ae", "as", "f", "fe", or "fs"
	utils::to_lower(target);

	std::string method_str = input.at("calculation_method"); // "fbts", "pbme" or "pbme-nh"
	utils::to_lower(method_str);
	if (method_str == "fbts")
		method = Method::fbts;
	else if (method_str == "pbme")
		method = Method::pbme;
	else
		method = Method::pbmenh;

	std::vector<std::vector<double>> H_S_temp = input.at("system_hamiltonian_[1/cm]");
	N_el = H_S_temp.size();
	H_S = Matrix<double>(N_el, N_el);
    eigen_matrix_from_stl(H_S, H_S_temp);

	std::vector<double> energy_disorder = input.at("energy_disorder_[1/cm]");
	// check for disorder: it is present if not all values of `energy_disorder` are zeros
	system_is_disordered = (std::count(energy_disorder.cbegin(), energy_disorder.cend(), 0) != N_el);
	disorder_distributions = std::vector<std::normal_distribution<double>>(N_el);
	// initialise the distributions if disorder is present
	if (system_is_disordered) {
		for (int i = 0; i < N_el; i++) {
			std::normal_distribution<double>::param_type parameters(H_S(i, i), energy_disorder[i]);
			disorder_distributions[i].param(parameters);
		}
	}

    N_el_actual = N_el + 1; // number of electronic levels including the ground state
    N_osc = input.at("spectral_density").at("oscillators_count");
    system_size = 4 * (target == "d" ? N_el : N_el_actual);
    bath_size = N_osc * N_el * 2;
    phase_space_size = system_size + bath_size;
}

/* Set relaxation time, oscillator frequencies and interaction strength parameters `d`. */
void Propagator::set_spectral_density_parameters() {
	// temperature
	const double T = input.at("temperature_[K]");
	constexpr auto k_B = 0.6950348; // Boltzmann constant [1/(cm*K)]
	beta = 1.0 / (T * k_B);

	// set oscillators' frequencies and the density of states
	std::string spectral_density_type = input.at("spectral_density").at("type"); // "Debye" or "B777"
	utils::to_lower(spectral_density_type);

	const bool discretisation_is_uniform = input.at("spectral_density").at("discretisation_is_uniform");
	omega_max = input.at("spectral_density").at("cutoff_frequency_[1/cm]");
	// `gamma` is needed even if the spectral density is B777 because it is used in the Hamilton equations to make time dimensionless
	gamma = input.at("spectral_density").at("relaxation_constant_[1/cm]");

	const auto domega = omega_max / N_osc; 
	omega = Array1D<double>(N_osc);
	Array1D<double> dos(N_osc); // density of states (density of frequencies), which is a function of omega
	if (!discretisation_is_uniform && spectral_density_type == "debye") { // if non-uniform discretisation of Debye has been requested
		const auto A = gamma * N_osc / std::atan(omega_max / gamma); // dos normalisation constant
		for (int i = 0; i < N_osc; i++) {
			omega(i) = gamma * std::tan( (i + 1) * gamma / A );
			dos(i) = A / (omega(i)*omega(i) + gamma*gamma);
		}
	} else { // if spectral density is not "Debye", or if uniform discretisation has been requested
		for (int i = 0; i < N_osc; i++) {
			omega(i) = (i + 1) * domega;
			dos(i) = 1.0 / domega;	// dos is constant in case of uniform discretisation
		}
	}

	// reorganisation energies
	Array1D<double> lambda_renormed(N_el);
	lambda = Array1D<double>(N_el);
	// spectral density
	Array2D<double> C(N_el, N_osc);
	if (spectral_density_type == "debye") {
		const double lambda_base = input.at("spectral_density").at("reorganisation_energy").at("base_value_[1/cm]");
		std::vector<double> lambda_coeffs = input.at("spectral_density").at("reorganisation_energy").at("coefficients");
		for (int i = 0; i < N_el; i++)
			lambda(i) = lambda_base * lambda_coeffs[i];
		// Renormalise `lambda` to improve the results slightly. It is required because when the spectral density is cut off,
		// its integral should give less than the value of the reorganisation energy.
		// Renormed values are used to calculate parameters `d` which are used in equations, but the class memeber `lambda`
		// stays the same and is used in the effective coupling theory.
		lambda_renormed = lambda * M_2_PI * std::atan(omega_max / gamma); // note that atan -> pi/2 as omega_max -> infinity
	
		// calculate the Debye spectral density
		for (int i = 0; i < N_el; i++)
			C.row(i) = 2.0 * lambda_renormed(i) * gamma * omega / (omega.square() + gamma*gamma);
	} else { // if (spectral_density_type == "B777")
		gamma = 53; // set to a typical value
		const auto B777_lambda = 39;
		lambda.setConstant(B777_lambda);
		// The spectral density B777 falls down quickly enough, so lambda renormalisation is not needed.
		// Cutting off at 500 1/cm already accounts for 98% of the reorganisation energy.
		lambda_renormed.setConstant(B777_lambda);

		// calculate the B777 spectral density
		const auto S = 0.5, s1 = 0.8, s2 = 0.5, omega1 = 0.56, omega2 = 1.9;
		for (int i = 0; i < lambda.size(); i++)
			C.row(i) = S*M_PI/(s1 + s2) * ( s1/std::pow(omega1, 4) * (-omega.sqrt()/std::sqrt(omega1)).exp() + 
											s2/std::pow(omega2, 4) * (-omega.sqrt()/std::sqrt(omega2)).exp() ) / 10080 * omega.pow(5);
	}
	// calculate parameters `d`
	d = Array2D<double>(N_el, N_osc);
	for (int i = 0; i < N_el; i++) {
		Array1D<double> d_temp2 = M_2_PI * C.row(i) / omega.square() / dos; // `d_temp` squared
		double A = (d_temp2 * omega).sum() / 2.0; // normalisation constant
		d.row(i) = std::sqrt(lambda_renormed(i) / A) * d_temp2.sqrt();
	}
}

/* Set propagation durations for the evolution and response function. */
void Propagator::set_propagation_durations() {
	// convert from ps to units of 1/gamma which are used in equations
	double time_interval = double(input.at("evolution_duration_[ps]")) / (ps_cm_converter / gamma); 
	// the user may enter 0 for the number of time points to get output every 1 fs; otherwise use the provided number
	evolution_points_count = int(input.at("evolution_points_count")) == 0 ? double(input.at("evolution_duration_[ps]")) * 1000 + 1
 														     		      : int(input.at("evolution_points_count")) + 1; // +1 to include the initial moment
	auto dt = time_interval / (evolution_points_count - 1);
	evolution_times.resize(evolution_points_count);
	for (int i = 0; i < evolution_points_count; i++)
		evolution_times[i] = i * dt;

	time_interval = double(input.at("response_duration_[ps]")) / (ps_cm_converter / gamma);
	response_points_count = int(input.at("response_points_count")) == 0 ? double(input.at("response_duration_[ps]")) * 1000 + 1
		 													     	    : int(input.at("response_points_count")) + 1; // +1 to include the initial moment
	auto response_dt = time_interval / (response_points_count - 1);
	response_times.resize(response_points_count);
	for (int i = 0; i < response_points_count; i++)
		response_times[i] = i * response_dt;
}

/* Calculate and output the density matrix; return the finish time, not counting the time spent for file output.
 * Calculation of dynamics is coded in a way so that the system is assumed to be in |1><1| state (the lowest state) initially
 * (but the number of states may be arbitrary). This simplifies the equations and allows for faster computations.
 */
auto Propagator::calculate_write_density_matrix(std::normal_distribution<double> &q_p_distribution,
										  		std::vector<std::normal_distribution<double>> &Q_P_distributions,
												double rel_tol) {
	// `mu0_E_stark` is unconditionally used when solving ODEs, so just pass a vector of zeros
	Vector<double> mu0_E_stark = Vector<double>::Zero(N_el_actual);

	// initialise the corresponding system of ODEs
	std::unique_ptr<AbstractOdeSystem> density_matrix_odes;
	constexpr auto gnd_included = false, skip_level0 = false;
	if (method == Method::fbts)
		density_matrix_odes = std::make_unique<FbtsOdeSystem>(gnd_included, skip_level0, N_el, N_osc, H_S, gamma, omega, d, mu0_E_stark);
	else if (method == Method::pbme)
		density_matrix_odes = std::make_unique<PbmeOdeSystem>(gnd_included, skip_level0, N_el, N_osc, H_S, gamma, omega, d, mu0_E_stark);
	else
		density_matrix_odes = std::make_unique<PbmenhOdeSystem>(gnd_included, skip_level0, N_el, N_osc, H_S, gamma, omega, d, mu0_E_stark);

	auto ode_stepper = odeint::make_dense_output<odeint::runge_kutta_dopri5<std::vector<double>>>(rel_tol/100, rel_tol);
	
	// for storing system density matrix
	Tensor<Complex> rho_S(evolution_points_count, N_el, N_el);
	rho_S.setZero();

	#pragma omp parallel
	{
#ifdef USE_OMP
		#pragma omp single
		{ 
			std::cout << "Using " << omp_get_num_threads() << " threads\n\n"; 
		}
		// a thread-private `rho_S` for accumulating thread-local result which is later used for reduction
		Tensor<Complex> rho_S_private(evolution_points_count, N_el, N_el);
		rho_S_private.setZero();
#else
		Tensor<Complex> &rho_S_private = rho_S;
#endif
		// A vector of phase space integration variables (initial conditions) in the following order:
		// y = {q1, q2, p1, p2, q'1, q'2, p'1, p'2, Q11, Q12, Q21, Q22, P11, P12, P21, P22}
		std::vector<double> y(phase_space_size);
		// "pointers" to where the corresponding variables begin in the `y` array
		const auto q = 0, p = N_el, q_prime = N_el * 2, p_prime = N_el * 3;
		// solution of the ODEs
		Array2D<double> y_t(evolution_points_count, system_size);

		// set up the generator, each thread will have its own one
		const auto seed = std::chrono::system_clock::now().time_since_epoch().count();
#ifdef USE_OMP		
		std::mt19937_64 generator{static_cast<std::mt19937_64::result_type>(seed + omp_get_thread_num())}; // the arguments are signed long long's, so, being explicit, we need a cast
#else
		std::mt19937_64 generator{static_cast<std::mt19937_64::result_type>(seed + thread_id)};
#endif
		#pragma omp for
		for (long long i = 0; i < N_mc; i++) {
			// generate the initial conditions (coordinates) q, p, q', p'
			for (int j = 0; j < system_size; j++) // system coordinates (q, p, q', p')
				y[j] = q_p_distribution(generator);
			// Iterate through the bath coordinates Q, P.
			// First `N_osc` iterations generate Q coordinates for the first site: Q(1,1)...Q(1,N_osc).
			// Next `N_osc` iterations generate P coordinates for the first site and use the same distributions, hence the mod.
			// Then we move on to the next site and again start with Q: Q(2,1)...Q(2,N_osc). Oscillators' frequencies are the same
			// so again the same distributions are used. 
			for (int j = system_size; j < phase_space_size; j++) // bath coordinates (Q, P)
				y[j] = Q_P_distributions[(j - system_size) % N_osc](generator);
			if (system_is_disordered)
				for (int j = 0; j < N_el; j++)
					H_S(j, j) = disorder_distributions[j](generator);

			// Calculate all the elements of `rho_S_private` at each time moment. Note that complex-conjugate formulae are used.			
			if (method == Method::fbts) {
				// we have to downcast `density_matrix_odes` because `integrate_times()` takes this argument by value,
				// and we cannot exploit polymorphism :(
				odeint::integrate_times(ode_stepper, *static_cast<FbtsOdeSystem*>(density_matrix_odes.get()),
										y, evolution_times.begin(), evolution_times.end(), 0.01, SaveStep(y_t, system_size));
				for (int j = 0; j < evolution_points_count; j++)
					for (int a = 0; a < N_el; a++)
						for (int b = 0; b < N_el; b++)
							rho_S_private(j, a, b) += (y_t(0, q)     - I*y_t(0, p))     * (y_t(0, q_prime)     + I*y_t(0, p_prime)) * 
													  (y_t(j, q + a) + I*y_t(j, p + a)) * (y_t(j, q_prime + b) - I*y_t(j, p_prime + b));
			} else { // if (method == Method::pbme || method == "pbme-nh")
				if (method == Method::pbme)
					odeint::integrate_times(ode_stepper, *static_cast<PbmeOdeSystem*>(density_matrix_odes.get()),
											y, evolution_times.begin(), evolution_times.end(), 0.01, SaveStep(y_t, system_size));
				else // if (method == "pbme-nh")
					odeint::integrate_times(ode_stepper, *static_cast<PbmenhOdeSystem*>(density_matrix_odes.get()),
											y, evolution_times.begin(), evolution_times.end(), 0.01, SaveStep(y_t, system_size));
				for (int j = 0; j < evolution_points_count; j++)
					for (int a = 0; a < N_el; a++)
						for (int b = 0; b < N_el; b++)
							rho_S_private(j, a, b) += (y_t(0, q) * y_t(0, q) + y_t(0, p) * y_t(0, p) - 0.5) *
													  (    y_t(j, q + a) * y_t(j, q + b) + y_t(j, p + a) * y_t(j, p + b) - 
													    I*(y_t(j, q + a) * y_t(j, p + b) - y_t(j, q + b) * y_t(j, p + a)) - (a == b ? 1.0 : 0) );
			}
		}
#ifdef USE_OMP
		#pragma omp critical
		{
			rho_S += rho_S_private;
		}
#endif
	}
	// Average the off-diagonal elements, and write the result above the diagonal.
	// The value below the diagonal remains less accurate but is not used during output.
	#pragma omp parallel for
	for (int t = 0; t < evolution_points_count; t++)
		for (int a = 0; a < N_el; a++)
			for (int b = a + 1; b < N_el; b++)
				rho_S(t, a, b) = (rho_S(t, a, b) + std::conj(rho_S(t, b, a))) / 2.0;

	const double normalisation = (method == Method::fbts ? N_mc * 4.0 : N_mc); // in the FBTS case, there's an additional factor of 4
	#pragma omp parallel for collapse(3)
	for (int t = 0; t < evolution_points_count; t++)
		for (int a = 0; a < N_el; a++)
			for (int b = 0; b < N_el; b++)
				rho_S(t, a, b) /= normalisation;

	auto finish_time = std::chrono::high_resolution_clock::now();

#ifdef USE_OMP
	std::string rho_S_filename = std::string{input.at("calculation_method")} + "_rho_S.txt";
#else
	std::string rho_S_filename = std::to_string(thread_id) + "_rho_S.temp"; // name the output file after the thread number
#endif

	std::ofstream rho_S_stream{rho_S_filename}; 
	rho_S_stream.precision(7);
	for (int t = 0; t < evolution_points_count; t++) {
		rho_S_stream << evolution_times[t] * (ps_cm_converter / gamma) << " ";	// time is converted from units of 1/gamma to ps
		for (int j = 0; j < N_el; j++) {
			for (int k = 0; k < N_el; k++) {
				if (j <= k)	// if we're on or above the diagonal, use element (j, k)
					rho_S_stream << rho_S(t, j, k).real() << " " << rho_S(t, j, k).imag() << " ";
				else // below the diagonal, use the corresponing element from above diagonal and make conjugation manually
					rho_S_stream << rho_S(t, k, j).real() << " " << -rho_S(t, k, j).imag() << " ";
			}
		}
		rho_S_stream << "\n";
	}

	return finish_time;
}

#ifdef USE_OMP
/* Print estimated execution time based on input parameters, and return start time. */
auto Propagator::estimate_execution_time() const {
	const auto start_time = std::chrono::high_resolution_clock::now();
	// convert `start_time` to C-style `time_t` object for convenient output
	const auto start_time_c = std::chrono::system_clock::to_time_t(start_time);
	// calculate approximate execution time
	const double evolution_duration = input.at("evolution_duration_[ps]"),
		   		 response_duration = input.at("response_duration_[ps]");
	const auto propagation_duration = (target == "d" ? evolution_duration : response_duration);
	// exponentiation of `N` accounts for the fact that bigger values of `N` induce higher ODE propagation accuracy,
	// which prolongs the overall caclulation time
	int predicted_execution_time_sec = double(input.at("reference_execution_time_[s]")) * std::pow(N_mc/1E5, 1.15) * N_osc/100 * omega_max/1000 *
		 						       propagation_duration/0.5 * N_el/2;
	if (target != "d") {
		// approximately account for the fact that calculation of spectra takes longer than calculation of dynamics
		predicted_execution_time_sec *= 1.5;
		// approximately account for the the time required to calculate the evolution
		if (target[0] == 'f')
			predicted_execution_time_sec += (predicted_execution_time_sec/response_duration * evolution_duration) * 0.8;
		if (target[1] == 's')
			predicted_execution_time_sec *= 2;
	}
    const auto predicted_finish_time = std::chrono::system_clock::to_time_t(start_time + std::chrono::seconds(predicted_execution_time_sec));
    std::cout << "Job started on " << ctime(&start_time_c)
		 	  << "Execution will require approximately " <<  predicted_execution_time_sec / 3600 << " h "
				 										 << (predicted_execution_time_sec % 3600) / 60 << " min "
														 << (predicted_execution_time_sec % 3600) % 60 << " s\n"
		 	  << "Will finish on " << ctime(&predicted_finish_time);
	return start_time;
}
#endif

#ifdef USE_OMP
/* Perform FFT on the given response function, and output the resulting spectrum. */
void Propagator::perform_fft(const Complex R[]) const {
	// The response function will be padded with zeros before transforming.
	// For that, find the next 5th power of 2 for `response_points_count` -- this will be the number of points used for FFT.
	const auto N_fft = ( 1 << (5 + int(std::log2(response_points_count))) );
	auto spectrum = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N_fft); // C-style allocation as recommended for FFTW
	for (int i = 0; i < response_points_count; i++)	// copy `R` to the beginning of `spectrum`
		spectrum[i][0] = R[i].real(), spectrum[i][1] = R[i].imag();
	for (int i = response_points_count; i < N_fft; i++)	// fill the rest of `spectrum` with zeros
		spectrum[i][0] = spectrum[i][1] = 0.0;

	// perform FFT
	const auto fft_direction = (target[0] == 'a' ? FFTW_BACKWARD : FFTW_FORWARD);
	auto plan = fftw_plan_dft_1d(N_fft, spectrum, spectrum, fft_direction, FFTW_ESTIMATE); // in-place transform
	fftw_execute(plan);
	fftw_destroy_plan(plan);

	// perform "fftshift"
	for (int i = 0; i < N_fft/2; i++)
		std::swap(spectrum[i][0], spectrum[i + N_fft/2][0]);

	// prepare frequencies
	const double dt = double(input.at("response_duration_[ps]")) / (response_points_count - 1) ; // get `dt` in ps
	const auto f_max = (M_PI / dt) * ps_cm_converter, // convert to 1/cm
			   df = 2.0 * f_max / N_fft;
	Array1D<double> frequencies(N_fft);
	for (int i = 0; i < N_fft; i++)
		frequencies[i] = (-f_max + df) + i * df;
	
	std::ofstream spectrum_stream{std::string{input.at("calculation_method")}+ "_" + target + "_spectrum.txt"};
	spectrum_stream.precision(7);
	for (int i = 0; i < N_fft; i++)
		spectrum_stream << frequencies[i] << " " << spectrum[i][0] << "\n";	

	fftw_free(spectrum);
}
#endif

/* Launch the calculation. */
void Propagator::calculate() {
	// make statistical distributions
	std::normal_distribution<double> q_p_distribution{0, method == Method::fbts ? 1 : M_SQRT1_2};
	// Each bath coordinate requires a separate distribution because each Q_ij and P_ij is multiplied by a different omega_ij
	// However, each (Q_ij, P_ij) pair is multiplied by the same omega_ij, so we can use the same distributions for each pair.
	// Further, since oscillators of the same frequency are assumed to interact with both sites, we also use the same distributions
	// for Q_ij and Q_kj as well as P_ij and P_kj. Thus, only N_osc distributions are required.
	std::vector<std::normal_distribution<double>> Q_P_distributions(N_osc);
	Array1D<double> sigma = ( (beta * omega / 2.0).tanh().inverse() / 2.0 ).sqrt();
	// initialise the distributions with corresponding sigma
	for (int i = 0; i < N_osc; i++) {
		std::normal_distribution<double>::param_type parameters{0.0, sigma[i]};
		Q_P_distributions[i].param(parameters);
	}

	// calculate the relative tolerance based on the number of trajectories, or use the user value 
	auto rel_tol = double(input.at("relative_tolerance")) == 0 ? 1.0 / std::sqrt(N_mc * 10)
 														       : double(input.at("relative_tolerance"));

	// initialise finish time (the initialisation value is actually not needed, but this way we can use `auto`)
	auto finish_time = std::chrono::high_resolution_clock::now();
	
#ifdef USE_OMP
	const auto start_time = estimate_execution_time();
#endif

	if (target == "d")
		finish_time = calculate_write_density_matrix(q_p_distribution, Q_P_distributions, rel_tol);
	else {		
		// response function; manual allocation to support OpenMP reduction
		auto R = new Complex[response_points_count];
		for (int i = 0; i < response_points_count; i++)
			R[i] = 0;

		calculate_response(R, q_p_distribution, Q_P_distributions, rel_tol);
		
		finish_time = std::chrono::high_resolution_clock::now();

#ifdef USE_OMP
		std::string response_filename = std::string{input.at("calculation_method")} + "_" + target + "_response.txt";
#else
		std::string response_filename = std::to_string(thread_id) + "_response.temp"; // name the output file after the thread number
#endif
		std::ofstream response_stream{response_filename}; 
		response_stream.precision(7);
		for (int i = 0; i < response_points_count; i++) // time is converted from units of 1/gamma to ps 
			response_stream << response_times[i] * (ps_cm_converter / gamma) << " "
							<< R[i].real() << " " << R[i].imag() << "\n";

#ifdef USE_OMP
		perform_fft(R);
#endif
		delete [] R;
	}

#ifdef USE_OMP
	const auto elapsed_time = std::chrono::duration_cast<std::chrono::seconds>(finish_time - start_time).count();
	// convert `finish_time` to C-style `time_t` object for convenient output
	const auto finish_time_c = std::chrono::system_clock::to_time_t(finish_time);
	std::cout << "Job finished on " << std::ctime(&finish_time_c)
	 	 	  << "Elapsed time: " <<  elapsed_time / 3600 << " h "
				 				  << (elapsed_time % 3600) / 60 << " min "
							 	  << (elapsed_time % 3600) % 60 << " s\n\a";
#endif								   
}

/* Calculate response without any external fields (target == "a" || target == "f"),
 * in the presence of an external electric field (target == "ae" || target == "fe"),
 * or the Stark response (target == "as" || target == "fs"), which is the difference with the field on and off.
 * Result is written to `R`.
 */
void Propagator::calculate_response(Complex R[], std::normal_distribution<double> &q_p_distribution,
                		        	std::vector<std::normal_distribution<double>> &Q_P_distributions,
									double rel_tol) {
	// read the dipole moments
	const std::vector<std::vector<double>> mu_transition = input.at("transition_dipole_moments_[D]");
	const std::vector<std::vector<double>> mu_static = input.at("static_dipole_moments_[D]");

	// initialise the electric fields
	const double E_stark_strength = double(input.at("stark_field_strength_[MV/cm]")) * 1.6792E+1; // convert MV/cm to 1/(D*cm)
	Vector<double> E_stark(3); E_stark << 0.0, E_stark_strength * M_SQRT1_2, E_stark_strength * M_SQRT1_2;
	Vector<double> E_light(3); E_light << 1.0/std::sqrt(3.0), std::sqrt(2.0/3.0), 0.0;

	const bool should_use_J_eff = input.at("should_use_J_eff"); // whether the equilibrium should be claculated using effective J
	const bool should_read_RDM = input.at("should_read_RDM"); // whether the equilibrium should be read from the input file
	Matrix<double> H_eff = Matrix<double>::Zero(N_el, N_el); // effective Hamiltonian for the effective J theory
	// equilibrium system density operator
	Matrix<Complex> rho_S_field_off = Matrix<Complex>::Zero(N_el_actual, N_el_actual);
	if (target[0] == 'a')
		rho_S_field_off(0, 0) = 1;	 // for absorption, set system to the ground state
	else if (target[0] == 'f') { // for fluorescence,
		if (should_use_J_eff) {  // the excited state equilibrium *with the field off* may be calculated using effective J
			for (int a = 0; a < N_el; a++) {
				for (int b = 0; b < N_el; b++) {
					if (a == b)
						H_eff(a, a) = H_S(a, a) - lambda(a);
					else
						H_eff(a, b) = H_S(a, b) * std::exp(-beta * (lambda(a) + lambda(b)) / 6);
				}
			}
			Matrix<double> rho_unnormalised = (-beta*H_eff).exp();
			rho_S_field_off.block(1, 1, N_el, N_el) = rho_unnormalised / rho_unnormalised.trace();
		} else if (should_read_RDM) { // read the excited state equilibrium *with the field off* from input
			// RDM is of size N_el x N_el, and its format is
			// Re(rho11), Im(rho11), Re(rho12), Im(rho12), ...
			std::vector<double> rho_S_temp = input.at("excited_state_RDM");
			// place it into `rho_S_field_off` (which is N_el_actual x N_el_actual) by skipping the first row and the first column.
			for (int i = 0; i < N_el*N_el; i++)
				rho_S_field_off(1 + i/N_el, 1 + i%N_el) = Complex{rho_S_temp[i*2], rho_S_temp[i*2 + 1]};
		}
	}

	// Initial dipole operator. It's a 2D matrix where each element is a 3-element vector of (x,y,z) projections,
	// so a 3D matrix is needed to store it.
	Tensor<double> mu0(N_el_actual, N_el_actual, 3);
	mu0.setZero();
	// assign input transition dipoles to corresponding elements of the dipole operator
	for (int i = 1; i < N_el_actual; i++)	// iterate through columns of the dipole operator, starting from 1
		for (int j = 0; j < 3; j++) // iterate through (x,y,z) components
			mu0(0, i, j) = mu0(i, 0, j) = mu_transition[i-1][j];
	// assign static moments
	for (int i = 0; i < N_el_actual; i++)
		for (int j = 0; j < 3; j++) // iterate through (x,y,z) components
			mu0(i, i, j) = mu_static[i][j];

	// statistical distributions for orientational averaging
	std::uniform_real_distribution<double> theta_distribution{0, M_PI};
	std::uniform_real_distribution<double> phi_chi_distribution{0, 2*M_PI};

	const auto target_is_stark = (target == "as" || target == "fs");

	auto ode_stepper = odeint::make_dense_output<odeint::runge_kutta_dopri5<std::vector<double>>>(rel_tol/100, rel_tol);

	#pragma omp parallel
	{
#ifdef USE_OMP			
		#pragma omp single
		{ 
			std::cout << "Using " << omp_get_num_threads() << " threads\n\n"; 
		}
#endif 
		Matrix<double> H_eff_private = H_eff;
		Matrix<double> rho_unnormalised(N_el, N_el);
		// excited state equilibrium with the field on
		Matrix<Complex> rho_S_field_on = Matrix<Complex>::Zero(N_el_actual, N_el_actual);

		// A vector of phase space integration variables (initial conditions) in the following order:
		// y = {q0, q1, q2, p0, p1, p2, q'0, q'1, q'2, p'0, p'1, p'2, Q11, Q12, Q21, Q22, P11, P12, P21, P22}
		std::vector<double> y0(phase_space_size);
		std::vector<double> y0_copy(phase_space_size); // needed for manual calulation of RDM and for Stark
		std::vector<double> y_eq(system_size); // equilibrium system coordinates, needed for manual calulation of RDM in case of PBME(-nH)
		// "pointers" to where the corresponding variables begin in the `y` array
		const auto q = 0, p = N_el_actual, q_prime = N_el_actual * 2, p_prime = N_el_actual * 3;
		// solution of the ODEs
		Array2D<double> y_t = Array2D<double>::Zero(response_points_count, phase_space_size);

		// set up the generator, each thread will have its own one
		const auto seed = std::chrono::system_clock::now().time_since_epoch().count();
#ifdef USE_OMP		
		std::mt19937_64 generator{static_cast<std::mt19937_64::result_type>(seed + omp_get_thread_num())};
#else
		std::mt19937_64 generator{static_cast<std::mt19937_64::result_type>(seed + thread_id)};
#endif
		// `mu0` converted to the Lab frame
		Tensor<double> mu0_lab(N_el_actual, N_el_actual, 3);
		mu0_lab.setZero();
		// the result of multiplication of `mu0_lab` by the polarisation vector
		Matrix<double> mu0_pol = Matrix<double>::Zero(N_el_actual, N_el_actual);
		// diagonal elements of `mu0_lab` multiplied by `E_stark`
		Vector<double> mu0_E_stark = Vector<double>::Zero(N_el_actual);
		// the result of multiplication of `mu0_lab` by `rho_S_field_off` and polarisation
		Matrix<Complex> mu0_pol_rho_S = Matrix<Complex>::Zero(N_el_actual, N_el_actual);

		// dipole operator that will be propagated and immediately multiplied by the polarisation vector
		Tensor<Complex> mu_t(response_points_count, N_el_actual, N_el_actual);
		mu_t.setZero();

		// initialise the system of ODEs for propagating the density matrix
		std::unique_ptr<AbstractOdeSystem> density_matrix_odes;
		auto gnd_included = false, skip_level0 = true;
		if (method == Method::fbts)
			density_matrix_odes = std::make_unique<FbtsOdeSystem>(gnd_included, skip_level0, N_el, N_osc, H_S, gamma, omega, d,	mu0_E_stark);
		else if (method == Method::pbme)
			density_matrix_odes = std::make_unique<PbmeOdeSystem>(gnd_included, skip_level0, N_el, N_osc, H_S, gamma, omega, d,	mu0_E_stark);
		else
			density_matrix_odes = std::make_unique<PbmenhOdeSystem>(gnd_included, skip_level0, N_el, N_osc, H_S, gamma, omega, d, mu0_E_stark);
		// initialise the system of ODEs for the response function
		std::unique_ptr<AbstractOdeSystem> response_odes;
		gnd_included = true, skip_level0 = false;
		if (method == Method::fbts)
			response_odes = std::make_unique<FbtsOdeSystem>(gnd_included, skip_level0, N_el_actual, N_osc, H_S, gamma, omega, d, mu0_E_stark);
		else if (method == Method::pbme)
			response_odes = std::make_unique<PbmeOdeSystem>(gnd_included, skip_level0, N_el_actual, N_osc, H_S, gamma, omega, d, mu0_E_stark);
		else
			response_odes = std::make_unique<PbmenhOdeSystem>(gnd_included, skip_level0, N_el_actual, N_osc, H_S, gamma, omega, d, mu0_E_stark);

		ConverterToLabFrame converter{0, 0, 0};

		// Prepare for the inner `s` loop. There will be at most 2 iterations: when `s == 0`, field is off, and when `s == 1`, field is on.
		// The total number of iterations and the number of the first iteration depend on the target.
		const auto iteration_count = (target_is_stark ? 2 : 1);
		const auto first_iteration = ((target == "ae" || target == "fe") ? 1 : 0);
		double sign = 1; // used to alternate between response addition and subtraction when calculating Stark

		// Whether RDM should be calculated using the FBTS/PBME. Note that using Jeff is not considered to be a "calculation" of RDM.
		const auto should_calculate_RDM = (target[0] == 'f' && !should_use_J_eff && !should_read_RDM);

		// teach OpenMP how to perform reduction on `Complex` variables
		#pragma omp declare reduction(+ : Complex : omp_out += omp_in) initializer(omp_priv = 0)
		#pragma omp for reduction(+ : R[0:response_points_count])
		for (long long i = 0; i < N_mc; i++) {
			// generate the initial conditions (coordinates) q, p, q', p', Q, P
			for (int j = 0; j < system_size; j++) // system coordinates (q, p, q', p')
				y0[j] = q_p_distribution(generator);
			for (int j = system_size; j < phase_space_size; j++) // bath coordinates (Q, P)
				y0[j] = Q_P_distributions[(j - system_size) % N_osc](generator);
			if (system_is_disordered)
				for (int j = 0; j < N_el; j++)
					H_S(j, j) = disorder_distributions[j](generator);

			if (target_is_stark) // Make a copy of `y0` if calculating Stark response. In that case, we first calculate the response without
				y0_copy = y0;    // the field, and `y0` is propagated. Then, we turn on the field and again need the same initial conditions.
			else if (should_calculate_RDM) // if calculating RDM manually, we need the initial system coordinates in the FBTS/PBME formula,
				std::copy_n(y0.cbegin(), system_size, y0_copy.begin());	// so copy only the system part of `y0`

			auto theta = theta_distribution(generator),
				 phi = phi_chi_distribution(generator),
				 chi = phi_chi_distribution(generator);
			converter.set_Phi(theta, phi, chi);
			converter.convert(mu0, mu0_lab);

			// Multiply `mu0_lab` by `E_light`, but calculate only the first (zeroth) row and the first column.
			// Diagonal elements are also ignored.
			for (int a = 0, b = 1; b < N_el_actual; b++) {
				mu0_pol(a, b) = mu0_lab(a, b, 0)*E_light(0) + mu0_lab(a, b, 1)*E_light(1) + mu0_lab(a, b, 2)*E_light(2);
				mu0_pol(b, a) = mu0_pol(a, b);
			}

			if (!should_calculate_RDM)
				mu0_pol_rho_S = mu0_pol * rho_S_field_off * std::sin(theta); // sin(theta) comes from the orientational averaging

			// perform at most two iterations; `s == 0` -- field off, `s == 1` -- field on
			for (int s = first_iteration; s < iteration_count; s++) {
				if (s == 0 && target_is_stark) { // first iteration, field is off
					sign = -1;
					mu0_E_stark.setZero(); // `E_stark = 0`, so the product should be set to zero,
				} else if (s == 1) {	// second iteration, field is on
					// multiply `mu0_lab` by `E_stark`
					for (int a = 0; a < N_el_actual; a++)
						mu0_E_stark(a) = -mu0_lab(a, a, 0)*E_stark(0) - mu0_lab(a, a, 1)*E_stark(1) - mu0_lab(a, a, 2)*E_stark(2);
					
					// For fluorescence, calculate equilibrium density matrix (with the field on) using effective coupling.
					// For absorption, this is not needed and `mu0_pol_rho_S` is the same on both iterations of the `s` loop.
					if (target[0] == 'f' && should_use_J_eff) {
						for (int a = 0; a < N_el; a++) // add `mu0_E_stark` to the diagonal of `H_eff`
							H_eff_private(a, a) = H_eff(a, a) + mu0_E_stark(a+1);
						rho_unnormalised = (-beta*H_eff_private).exp();
						rho_S_field_on.block(1, 1, N_el, N_el) = rho_unnormalised / rho_unnormalised.trace();
						mu0_pol_rho_S = mu0_pol * rho_S_field_on * std::sin(theta);
					}
					
					if (target_is_stark) {
						sign = 1;
						y0 = y0_copy; // restore the initial conditions
					}
				}

				if (method == Method::fbts) {
					if (target[0] == 'f') {
						// Propagate oscillators until equilibrium.
						// Here, only the final step is needed, so we do not provide an observer. The final result is written to `y0`.
						// `y0` contains all coordinates, but those corresponding to level 0 will be ignored during propagation.
						odeint::integrate_adaptive(ode_stepper, *static_cast<FbtsOdeSystem*>(density_matrix_odes.get()),
												   y0, evolution_times.front(), evolution_times.back(), 0.01);
						if (should_calculate_RDM) { // if have to calculate RDM manually
							// Calculate the RDM ignoring the zeroth row and column, hence `+1` for the `y0_copy` elements
							// (there is no `+1` for the elements `y0` because we start from a, b = 1 in the loop).
							// We use `y0_copy` as the initial (unpropogated) coordinates, while `y0` are actually propagated ones.
							// Note that here `rho_S_field_on` is used as a convenient thread-private variable, and it does not reflect
							// whether the field is off or on. The same variable is used in both cases.
							for (int a = 1; a < N_el_actual; a++) {
								for (int b = 1; b < N_el_actual; b++) {
									rho_S_field_on(a, b) = (y0_copy[q + 1] - I*y0_copy[p + 1]) * (y0_copy[q_prime + 1] + I*y0_copy[p_prime + 1]) * 
												  		   (y0[q + a]      + I*y0[p + a])      * (y0[q_prime + b]      - I*y0[p_prime + b]) / 4.0;
									if (a > b) {	// when in the lower triangle, average the coherences
										rho_S_field_on(a, b) = (rho_S_field_on(a, b) + std::conj(rho_S_field_on(b, a))) / 2.0;
										rho_S_field_on(b, a) = std::conj(rho_S_field_on(a, b));
									}
								}
							}
							mu0_pol_rho_S = mu0_pol * rho_S_field_on * std::sin(theta);
							for (int j = 0; j < system_size; j++) // generate new system coordinates
								y0[j] = q_p_distribution(generator);
						}
					}

					// propagate oscillators for the response function
					odeint::integrate_times(ode_stepper, *static_cast<FbtsOdeSystem*>(response_odes.get()),
											y0, response_times.begin(), response_times.end(), 0.01, SaveStep(y_t, system_size));

					// calculate the first row and the first column of `mu_t` at each time moment
					for (int t = 0; t < response_points_count; t++) {
						for (int a = 0, b = 1; b < N_el_actual; b++) { // iterate through columns
							mu_t(t, a, b) = 0;
							for (int m = 0, n = 1; n < N_el_actual; n++) // calculate only element (a, b), element (b, a) is the same at this point
								mu_t(t, a, b) += (y_t(t, q + m) - I*y_t(t, p + m)) * (y_t(t, q_prime + n) + I*y_t(t, p_prime + n)) * mu0_pol(m, n) +
								 				 (y_t(t, q + n) - I*y_t(t, p + n)) * (y_t(t, q_prime + m) + I*y_t(t, p_prime + m)) * mu0_pol(n, m);
							mu_t(t, b, a) = mu_t(t, a, b);
							mu_t(t, a, b) *= (y_t(0, q + a) + I*y_t(0, p + a)) * (y_t(0, q_prime + b) - I*y_t(0, p_prime + b)) / 4.0;
							mu_t(t, b, a) *= (y_t(0, q + b) + I*y_t(0, p + b)) * (y_t(0, q_prime + a) - I*y_t(0, p_prime + a)) / 4.0;
							mu_t(t, a, b) = (mu_t(t, a, b) + std::conj(mu_t(t, b, a))) / 2.0;
							mu_t(t, b, a) = std::conj(mu_t(t, a, b));
						}
					}
				} else { // if (method == Method::pbme || method == Method::pbmenh)
					if (method == Method::pbme) {
						if (target[0] == 'f') {
							odeint::integrate_adaptive(ode_stepper, *static_cast<PbmeOdeSystem*>(density_matrix_odes.get()),
													   y0, evolution_times.front(), evolution_times.back(), 0.01);
							if (should_calculate_RDM) {
								std::move(y0.begin(), y0.begin()+system_size, y_eq.begin()); // save the equilibrium system coordinates
								for (int j = 0; j < system_size; j++) // generate new system coordinates
									y0[j] = q_p_distribution(generator);
							}
						}
						odeint::integrate_times(ode_stepper, *static_cast<PbmeOdeSystem*>(response_odes.get()),
												y0, response_times.begin(), response_times.end(), 0.01, SaveStep(y_t, system_size));
					} else { // if (method == Method::pbmenh)
						if (target[0] == 'f') {
							odeint::integrate_adaptive(ode_stepper, *static_cast<PbmenhOdeSystem*>(density_matrix_odes.get()),
													   y0, evolution_times.front(), evolution_times.back(), 0.01);
							if (should_calculate_RDM) {
								std::move(y0.begin(), y0.begin()+system_size, y_eq.begin());
								for (int j = 0; j < system_size; j++)
									y0[j] = q_p_distribution(generator);
							}
						}
						odeint::integrate_times(ode_stepper, *static_cast<PbmenhOdeSystem*>(response_odes.get()),
												y0, response_times.begin(), response_times.end(), 0.01, SaveStep(y_t, system_size));
					}

					if (should_calculate_RDM) {
						for (int a = 1; a < N_el_actual; a++) {
							for (int b = 1; b < N_el_actual; b++) {
								rho_S_field_on(a, b) = (y0_copy[q + 1] * y0_copy[q + 1] + y0_copy[p + 1] * y0_copy[p + 1] - 0.5) *
											  		   (   y_eq[q + a] * y_eq[q + b] + y_eq[p + a] * y_eq[p + b] - 
											   			I*(y_eq[q + a] * y_eq[p + b] - y_eq[q + b] * y_eq[p + a]) - (a == b ? 1.0 : 0) );
								if (a > b) {
									rho_S_field_on(a, b) = (rho_S_field_on(a, b) + std::conj(rho_S_field_on(b, a))) / 2.0;
									rho_S_field_on(b, a) = std::conj(rho_S_field_on(a, b));
								}
							}
						}
						mu0_pol_rho_S = mu0_pol * rho_S_field_on * std::sin(theta);
					}

					for (int t = 0; t < response_points_count; t++) {
						for (int a = 0, b = 1; b < N_el_actual; b++) {
							mu_t(t, a, b) = 0;
							for (int m = 0, n = 1; n < N_el_actual; n++) {
								mu_t(t, a, b) += (   y_t(t, q + m) * y_t(t, q + n) + y_t(t, p + m) * y_t(t, p + n) + 
												  I*(y_t(t, q + m) * y_t(t, p + n) - y_t(t, q + n) * y_t(t, p + m)) ) * mu0_pol(m, n) +
												 (   y_t(t, q + n) * y_t(t, q + m) + y_t(t, p + n) * y_t(t, p + m) + 
												  I*(y_t(t, q + n) * y_t(t, p + m) - y_t(t, q + m) * y_t(t, p + n)) ) * mu0_pol(n, m);
							}
							mu_t(t, b, a) = mu_t(t, a, b);
							mu_t(t, a, b) *=    y_t(0, q + a) * y_t(0, q + b) + y_t(0, p + a) * y_t(0, p + b) - 
											 I*(y_t(0, q + a) * y_t(0, p + b) - y_t(0, q + b) * y_t(0, p + a));
							mu_t(t, b, a) *=    y_t(0, q + b) * y_t(0, q + a) + y_t(0, p + b) * y_t(0, p + a) - 
											 I*(y_t(0, q + b) * y_t(0, p + a) - y_t(0, q + a) * y_t(0, p + b));
							mu_t(t, a, b) = (mu_t(t, a, b) + std::conj(mu_t(t, b, a))) / 2.0;
							mu_t(t, b, a) = std::conj(mu_t(t, a, b));
						}
					}
				}

				// for each time moment, multiply `mu0_pol_rho_S` by `mu_t`, but calculate only diagonal elements,
				// and sum them into `R` to get the trace, which is the final result
				for (int t = 0; t < response_points_count; t++)
					for (int a = 0; a < N_el_actual; a++)
						for (int b = 0; b < N_el_actual; b++)
							R[t] += sign * mu_t(t, a, b) * mu0_pol_rho_S(b, a);
			}
		}
	}
	for (int t = 0; t < response_points_count; t++)
		R[t] *= M_PI_2 / N_mc; // pi/2 comes from the angular averaging
}