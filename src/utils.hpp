#pragma once

#include <map>
#include <string>
#include <cctype> // tolower

namespace utils {
    /* Return a map of flags (format: `-x value`) and the correspoding values, both as strings. */
    inline auto parse_args(int argc, const char *argv[]) {
        std::map<std::string, std::string> flags;
        for (int i = 1; i < argc-1; i++) {
            auto &flag = argv[i]; // for convenience
            if (flag[0] == '-') { // a valid flag is found
                flags[flag] = argv[i+1]; // store the corresponding value
                i++; // skip next token, which is a value
            }
        }
        return flags;
    }

    /* Convert a given string to lowercase. */
    inline void to_lower(std::string &s) {
        for (auto &&c : s)
            c = std::tolower(c);
    }
}
