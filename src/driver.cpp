#include <iostream>
#include <map>
#include <string>
#include "utils.hpp" // parse_args
#include "Propagator.hpp"

/* Entry point of the QuaCla program.
 * Usage:
 *     OpenMP version: quacla.exe [options]
 * 	       Options:
 *             -j <threads>		Number of threads (jobs) to spawn. Defaults to the maximum available
 * 				    			number of threads.
 *  		   -f <file>		Input filename. Defaults to "input.json".
 *     Distributed version: quacla.exe <thread-id> <threads-count> [-f <file>]
 * 	       Note that the `-f` option must come last.
 */
int main(int argc, const char *argv[]) {
	auto thread_id = 0;
	auto threads_count = 0;
	std::string input_filename = "input.json";
	auto flag_map = utils::parse_args(argc, argv);
#ifdef USE_OMP
	if (flag_map.count("-j"))
		threads_count = std::stoi(flag_map["-j"]);
#else
	if (argc >= 3) {
		thread_id = std::stoi( std::string{argv[1]} ); // passed ID of the current thread
		threads_count = std::stoi( std::string{argv[2]} ); // passed total number of threads
	} else {
		std::cout << "Not enough input arguments, 2 are required.\nTerminating.";
		return 1;
	}
#endif
	if (flag_map.count("-f"))
		input_filename = flag_map["-f"];
		
	Propagator propagator{input_filename, thread_id, threads_count};
	propagator.calculate();

	return 0;
}